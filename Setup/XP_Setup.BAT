@echo off
echo 准备安装到%~d0盘。要安装到其他盘请自行修改该文件！
echo 请确保kernel在%~d0盘system目录下！
echo 安装前我们将把BOOT整个文件夹和bootmgr 拷贝到c:\
pause

ver | find "XP" > NUL && goto START
ver | find "2000" > NUL && goto START
ver | find "7" > NUL && goto 102K
ver | find "8" > NUL && goto 102K
ver | find "10" > NUL && goto 102K
goto EXIT


:102K
echo WIN7或WIN8或WIN10用户请运行7_Setup.bat安装系统！
goto EXIT

:START

::if not exist %~d0\boot.wim goto EXIT
echo 正在安装Nowes OS 2.0，请稍后...
echo.
echo Copyright (c) 2010-2015 Jiashisoft Ltd. All rights reserved.
echo.
copy "%~d0\boot" "c:\"
copy "%~d0\bootmgr" "c:\"

bootsect /nt60 c: /force /mbr 

bcdedit /store c:\boot\bcd /delete {bootmgr}  /f
bcdedit /store c:\boot\bcd /delete {ad6c7bc8-fa0f-11da-8ddf-0013200354d8}  /f
bcdedit /store c:\boot\bcd /delete {572bcd56-ffa7-11d9-aae0-0007e994107d} /f
bcdedit /store c:\boot\bcd /delete {ntldr}  /f

bcdedit /store c:\boot\bcd /create {bootmgr} /d "Windows Boot Manager"                  
bcdedit /store c:\boot\bcd  /set {bootmgr} device boot                            
bcdedit /store c:\boot\bcd  /timeout 3                                                  
bcdedit /store c:\boot\bcd  /set {bootmgr} locale "zh-CN"  


bcdedit.exe /store c:\boot\bcd -create  {ntldr} -d "WindowsXP" 
bcdedit.exe /store c:\boot\bcd -set  {ntldr} device partition=C:
bcdedit.exe /store c:\boot\bcd -set  {ntldr} path \ntldr
bcdedit.exe /store c:\boot\bcd -displayorder {ntldr} /addlast

bcdedit.exe /store c:\boot\bcd -set {bootmgr} locale "zh-CN"
rem bcdedit.exe /store c:\boot\bcd -set {current} locale "zh-CN"
bcdedit.exe /store c:\boot\bcd -set {default} locale "zh-CN"
bcdedit.exe /store c:\boot\bcd -set {memdiag} locale "zh-CN"

REM
REM {ad6c7bc8-fa0f-11da-8ddf-0013200354d8} 
REM
set RAMDISK_OPTIONS={ad6c7bc8-fa0f-11da-8ddf-0013200354d8}
REM
REM {572bcd56-ffa7-11d9-aae0-0007e994107d}, GUID for the WinPE boot entry
REM
set WINPE_GUID={572bcd56-ffa7-11d9-aae0-0007e994107d}
REM
REM Set Timeout
REM
bcdedit.exe -timeout 30
REM
REM Create Ramdisk device options for the boot.sdi file
REM
bcdedit.exe -create %RAMDISK_OPTIONS% -d "NowesOS" -device
bcdedit.exe -set %RAMDISK_OPTIONS% ramdisksdidevice partition=%~d0
bcdedit.exe -set %RAMDISK_OPTIONS% ramdisksdipath \boot\boot.sdi
REM
REM Create WinRE boot entry
REM
bcdedit.exe -create %WINPE_GUID% -d "NowesOS" -application OSLOADER
bcdedit.exe -set %WINPE_GUID% device ramdisk=[%~d0]\system\Kernel,%RAMDISK_OPTIONS%
bcdedit.exe -set %WINPE_GUID% path \windows\system32\boot\winload.exe
bcdedit.exe -set %WINPE_GUID% osdevice ramdisk=[%~d0]\system\kernel,%RAMDISK_OPTIONS%
bcdedit.exe -set %WINPE_GUID% systemroot \windows
bcdedit.exe -set %WINPE_GUID% detecthal yes
bcdedit.exe -set %WINPE_GUID% nx optin
bcdedit.exe -set %WINPE_GUID% winpe yes
bcdedit.exe -set %WINPE_GUID% locale "zh-CN" 
bcdedit.exe -displayorder %WINPE_GUID% /addlast
echo NowesOS安装完毕。
pause

:EXIT
pause