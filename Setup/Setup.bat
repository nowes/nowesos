@ echo off
echo.
echo Copyright (c) 2010-2015 Jiashisoft Ltd. All rights reserved.
echo.

ver | find "7" > NUL && goto NT6
ver | find "8" > NUL && goto NT6
ver | find "10" > NUL && goto NT6
ver | find "XP" > NUL && goto NT5
ver | find "2000" > NUL && goto NT5

:NT6
%~d0\7_Setup.bat

:NT5
%~d0\XP_Setup.bat